---
title: "The GTK-Doc Project: Requirements"
nav: requirements

navlinks: gtk-doc
bodyclass: gtkdoc
---
## Basic GTK-Doc Requirements
The main scripts are written in <a href="http://www.perl.com/" class="external">Perl</a>.

## XML Generation
For writing XML files you will need the following packages. This is highly recommended!
- <a href="http://www.oasis-open.org/docbook/" class="external">DocBook XML DTD</a>
- <a href="http://docbook.sourceforge.net/" class="external">DocBook XSL Stylesheets</a>
- <a href="http://xmlsoft.org/" class="external">libxslt &amp; libxml2</a>

## SGML Generation
For writing SGML files you will need the following packages. This is not actively maintained anymore.
- <a href="http://www.oasis-open.org/docbook/" class="external">DocBook SGML DTD</a>
- <a href="http://docbook.sourceforge.net/" class="external">DocBook DSSSL Stylesheets</a>
- <a href="http://www.jclark.com/jade" class="external">Jade</a> or <a href="http://sourceforge.net/projects/openjade" class="external">OpenJade</a>

Most distributions now have packages for all of these, so it is strongly advised that you grab those if possible.