---
nav: features
title: GTK+ Features
---
<div class="bubbles left">

<div class="bubble" id="Stability" markdown="1">
## Stability
GTK+ has been developed for over a decade to be able to deliver the enticing features and superb performance that it brings to your application development. GTK+ is supported by a large community of developers and has core maintainers from companies such as [Red Hat](http://www.redhat.com/), [Novell](http://www.novell.com/), [Lanedo](http://www.lanedo.com/gtk+.html), [Codethink](http://www.codethink.co.uk/), [Endless Mobile](https://endlessm.com/) and [Intel](http://www.intel.com/).
</div>

<div class="bubble" id="LanguageBindings" markdown="1">
## Language Bindings
GTK+ is available in many other programming languages thanks to the [language bindings](language-bindings.html) available. This makes GTK+ quite an attractive toolkit for application development.
</div>

<div class="bubble" id="Interfaces">
<div markdown="1">
## Interfaces
GTK+ has a comprehensive collection of core widgets and interfaces for use in your application.
</div>
		<div class="figure">
			<a href="images/features/twf.png" class="image"><img src="images/features/thumbnail-twf.png" alt="The Widget Factory" /></a>
		</div>
<div markdown="1">
- Windows (normal window or dialog, about and assistant dialogs)
- Displays (label, image, progress bar, status bar)
- Buttons and toggles (check buttons, radio buttons, toggle buttons and link buttons)
- Numerical (horizontal or vertical scales and spin buttons) and text data entry (with or without completion)
- Multi-line text editor
- Tree, list and icon grid viewer (with customizable renderers and model/view separation)
- Combo box (with or without an entry)
- Menus (with images, radio buttons and check items)-
- Toolbars (with radio buttons, toggle buttons and menu buttons)
- GtkBuilder (creates your user interface from XML)
- Selectors (color selection, file chooser, font selection)
- Layouts (tabulated widget, table widget, expander widget, frames, separators and more)
- Status icon (notification area on Linux, tray icon on Windows)
- Printing widgets
- Recently used documents (menu, dialog and manager)
</div>
</div>
</div>
<div class="bubbles right">

<div class="bubble" id="CrossPlatform">
<div markdown="1">
## Cross Platform
Originally GTK+ was developed for the X Window System but it has grown over the years to include backend support for other well known windowing systems. Today you can use GTK+ on:
</div>
	<ul class="platform_list">
		<li class="linux"><a href="download/linux.html">GNU/Linux and Unix</a></li>
		<li class="windows"><a href="download/windows.html">Windows</a></li>
		<li class="macos"><a href="download/macos.html">Mac OS X</a></li>
	</ul>
</div>

<div class="bubble" id="Accommodating" markdown="1">
## Accommodating
GTK+ caters for a number features that today's developers are looking for in a toolkit including:
- Native look and feel
- Theme support
- Thread safety
- Object oriented approach
- Internationalization
- Localization
- Accessibility
- Bidirectional text support (LTR/RTL, Left To Right/Right To Left)
- UTF8 support
- Documentation
</div>

<div class="bubble" id="Foundations" markdown="1">
## Foundations
GTK+ is built on top of GLib. GLib provides the fundamental algorithmic language constructs commonly duplicated in applications. This library has features such as: (this list is not a comprehensive list)
- Object and type system
- Main loop
- Dynamic loading of modules (i.e. plug-ins)
- Thread support
- Timer support
- Memory allocator
- Threaded Queues (synchronous and asynchronous)
- Lists (singly linked, doubly linked, double ended)
- Hash tables
- Arrays
- Trees (N-ary and binary balanced)
- String utilities and charset handling
- Lexical scanner and XML parser
- Base64 (encoding & decoding)
</div>

<div class="bubble" id="Mobile" markdown="1">
## Mobile
The GMAE (GNOME Mobile & Embedded) initiative has advanced the use, development and commercialization of GNOME components as a mobile and embedded user experience platform. It has brought together industry leaders, expert consultants, key developers and the community and industry organizations they represent. As a direct result of this, GTK+ has features pertaining to mobile and embedded platform requirements.

GTK+ has been involved in a number of embedded initiatives over the past few years including the development of:
- Nokia [770](http://europe.nokia.com/A4145104) / [N800](http://web.nseries.com/products/#l=products,n800) / [N810](http://web.nseries.com/products/#l=products,n810) / [N900](http://maemo.nokia.com/n900/)
- [One Laptop Per Child Project](http://www.laptop.org/)
- [OpenMoko](http://www.openmoko.com/)
</div>

</div>