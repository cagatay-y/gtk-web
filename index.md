---
nav: about
title: The GTK+ Project
---
## What is GTK+, and how can I use it?
GTK+, or the GIMP Toolkit, is a multi-platform toolkit for creating graphical user interfaces. Offering a complete set of widgets, GTK+ is suitable for projects ranging from small one-off tools to complete application suites.

### Where can I use it?
Everywhere! GTK+ is cross-platform and boasts an easy to use API, speeding up your development time. Take a look at the [screenshots](screenshots.html) to see a number of platforms GTK+ will run.

### What languages are supported?
GTK+ is written in C but has been designed from the ground up to support a [wide range of languages](language-bindings.html), not only C/C++. Using GTK+ from languages such as Perl and Python (especially in combination with the [Glade GUI builder](http://glade.gnome.org/)) provides an effective method of rapid application development.

### Are there any licensing restrictions?
GTK+ is free software and part of the [GNU Project](http://www.gnu.org/). However, the licensing terms for GTK+, the [GNU LGPL](http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html), allow it to be used by all developers, including those developing proprietary software, without any license fees or royalties.

Get an [overview](overview.html) of GTK+. Understand who started it, the basic architecture and why we use the license we do.

GTK+ has been involved in many [projects](http://www.gtk-apps.org/) and some big platforms. To get a glimpse of what people think of GTK+ and how it has been used in commercial projects, [read the success stories…](commerce.html)

To find out how more about what GTK+ can do for you, visit our [features](features.html) page. If you want to [contribute](development.html), you are more than welcome.