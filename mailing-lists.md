---
nav: development
title: GTK+ Mailing Lists
---
## Mailing Lists
There are several GTK+ mailing lists. With ALL of these mailing lists, if you want to subscribe, all you need to do is to go to the subscribe url for each mailing list or visit the list information page detailed below:

For information on how to use the mailing lists, see the [mailing list FAQ](http://mail.gnome.org/).

### gtk-list@gnome.org
This is the main mailing list and probably the one on which you should ask questions about GTK+.

This list is for general topics related to GTK+, including everything from discussion of proposed new features to questions about compiling GTK+.

Mailing list: <a href="mailto:gtk-list-request@gnome.org?subject=subscribe" class="email">Subscribe</a>, <a href="http://mail.gnome.org/mailman/listinfo/gtk-list" class="info">Information</a>, <a href="http://mail.gnome.org/archives/gtk-list/" class="archives">Archives</a>

### gtk-app-devel-list@gnome.org
Discussion on this list is oriented toward developing applications with GTK+. This has been a lower traffic list which is good for asking simple GTK+ questions.

Mailing list: <a href="mailto:gtk-app-devel-list-request@gnome.org?subject=subscribe" class="email">Subscribe</a>, <a href="http://mail.gnome.org/mailman/listinfo/gtk-app-devel-list" class="info">Information</a>, <a href="http://mail.gnome.org/archives/gtk-app-devel-list/" class="archives">Archives</a>

### gtk-devel-list@gnome.org
This list is for developers of the **core** GTK+ library to discuss GTK+ implementation. GTK+ application development and general GTK+ questions should **not** be asked on this list. The gtk-app-devel-list mailing list is more appropriate for that.

Bug reports and requests for new features should generally be entered in [GitLab](https://gitlab.gnome.org/GNOME/gtk) instead of sent to this list.

Mailing list: <a href="mailto:gtk-devel-list-request@gnome.org?subject=subscribe" class="email">Subscribe</a>, <a href="http://mail.gnome.org/mailman/listinfo/gtk-devel-list" class="info">Information</a>, <a href="http://mail.gnome.org/archives/gtk-devel-list/" class="archives">Archives</a>

### gtk-doc-list@gnome.org
This list is for discussion of the development of the gtk-doc documentation tool. Discussion about improving the GTK+ documentation itself belongs on gtk-devel-list.

Specific suggestions for the documentation should be put in [Bugzilla](http://bugzilla.gnome.org).

Mailing list: <a href="mailto:gtk-doc-list-request@gnome.org?subject=subscribe" class="email">Subscribe</a>, <a href="http://mail.gnome.org/mailman/listinfo/gtk-doc-list" class="info">Information</a>, <a href="http://mail.gnome.org/archives/gtk-doc-list/" class="archives">Archives</a>

### gtk-i18n-list@gnome.org
This list is intended for discussion of the internationalization and localization of GTK+ itself and of GTK+ applications.

Mailing list: <a href="mailto:gtk-i18n-list-request@gnome.org?subject=subscribe" class="email">Subscribe</a>, <a href="http://mail.gnome.org/mailman/listinfo/gtk-i18n-list" class="info">Information</a>, <a href="http://mail.gnome.org/archives/gtk-i18n-list/" class="archives">Archives</a>

### gtk-perl-list@gnome.org
This list is intended for discussion of the usage of GTK+ with Perl.

Mailing list: <a href="mailto:gtk-perl-list-request@gnome.org?subject=subscribe" class="email">Subscribe</a>, <a href="http://mail.gnome.org/mailman/listinfo/gtk-perl-list" class="info">Information</a>, <a href="http://mail.gnome.org/archives/gtk-perl-list/" class="archives">Archives</a>

### language-bindings@gnome.org
This list is intended for discussion among creators of language bindings.

Mailing list: <a href="mailto:language-bindings-request@gnome.org?subject=subscribe" class="email">Subscribe</a>, <a href="http://mail.gnome.org/mailman/listinfo/language-bindings" class="info">Information</a>, <a href="http://mail.gnome.org/archives/language-bindings/" class="archives">Archives</a>