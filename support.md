---
nav: support
title: GTK+ Support
---
<div class="sidebar" markdown="1">
## FAQ
For information about frequently asked questions, please [visit the FAQ](https://developer.gnome.org/gtk3/stable/gtk-question-index.html)

## Documentation
Visit our [documentation](documentation.html) page for books, tutorials, articles and presentations about GTK+.

## Get Involved
For support and details about how to get involved, see our [development](development.html) page.
</div>

## Get Assistance
GTK+ is supported by a number of businesses and they provide services, experience and skills to help the large community of GTK+ users produce innovative customer solutions in multiple industries and devices (like mobile and embedded).

Consultancy partners which support GTK+ offer a range of business services including:
- [Consultation](#consultation)
- [Hardware integration](#hardware-integration)
- [Porting](#porting)
- [Application development](#application-development)
- [Patch set back folding](#patch-set-back-folding)
- [Training](#training)

### Consultancy Partners
<table>
    <tr>
        <th>Companies</th>
	    <th>Websites</th>
	</tr>
	{% for business in site.data.businesses %}
	<tr>
        <td>{{ business.company }}</td>
        <td><a href="{{ business.website }}">{{ business.website }}</a></td>
	</tr>
	{% endfor %}
	<tr><td colspan="2"><div id="note">Not listed above? Want to be? Contact the gtk-devel <a href="/mailing-lists.php">mailing list</a>.</div></td></tr>
</table>

### Consultation
Perhaps your business is looking for consultancy around using GTK+. If this is what you&apos;re looking for, our consultancy partners affilliated with GTK+ can offer these services. This can range from development, to support, to even queries about licensing and how to integrate the toolkit into your existing solutions and much more!

### Hardware Integration
With changes happening all the time in hardware, GTK+ is constantly adapting to the requests for newer and richer user interface features. One great example of those is Multi-Touch support. Businesses invested in GTK+ have been at the forefront of implementing this hardware support and working with other projects up and down the Linux stack to make that happen (such as Xorg).

### Porting
Porting can cover a range of areas. Businesses supporting GTK+ offer porting applications to GTK+ from other toolkits and also porting areas of the toolkit to specific platforms (for example, Windows 64bit support).

### Application Development
Should you need application development for your solution to be deliverable cross platform, GTK+ has all the tools you need to do that. Most of the businesses who are involved with GTK+ have decades of experience on offer to assist you.

### Patch Set Back-Folding
It's not uncommon for companies to start forking a project to customize it and perhaps to keep it in-house. This can lead to wanting new features from an upstream project like GTK+ being available in your forked version. If you need help back-folding patch sets into upstream GTK+ or suggesting new features into the main stream, we have a support network that can help you do that.

### Training
There are various training opportunities on offer. Everything from writing an application with GTK+ to developing and customizing GTK+. If this is what you&apos;re looking for, there are businesses out there that can help you do that.